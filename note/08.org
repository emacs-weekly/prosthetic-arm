#+TITLE: Document Driven Configuration 8
#+SETUPFILE: ./theme.org

* 窗口管理
* 删除当前窗口
C-x 0
* 删除其他窗口
C-x 1
* 水平切分窗口
C-x 2
* 垂直切分窗口
C-x 3
* 切换到指定窗口
M-o

有大于两个窗口的话会出现对应的英文字母，输入字母跳转到对应窗口。
#+begin_src emacs-lisp
  (use-package ace-window
    :straight t
    :config
    (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 5.0)))))
    :bind
    ("M-o" . ace-window))
#+end_src
* 恢复窗口布局
C-c left-arrow
#+begin_src emacs-lisp
  (use-package winner-mode
    :straight nil
    :hook (after-init . winner-mode))
#+end_src
* 重启后恢复窗口布局
#+begin_src emacs-lisp
  (desktop-save-mode 1)
#+end_src
