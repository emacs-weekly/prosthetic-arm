#+TITLE: Document Driven Configuration 28
#+SETUPFILE: ./theme.org

* Emacs 的编程实践

分解每一个步骤并优化。

* 初始化项目

项目初始文件有：

+ index.html
+ package.json
+ style.css

** 自己写脚本
优点：是可以完全自定义，

缺点：功能不如专业的构建工具大而全，缺乏后续支持

** 使用内置 shell 调用现有工具
优点：原生工具支持

缺点：需要自定义的地方修改起来不方便，需要熟悉相关环境

** eshell 的问题

对 TUI 的支持不好，无法对选项进行选择，因此使用 vterm

#+begin_src emacs-lisp
  (use-package vterm
    :straight t
    :config
    (setq vterm-shell (executable-find "fish")
	  vterm-max-scrollback 10000))
#+end_src

* 启动项目

直接在 shell 中运行 ~yarn dev~, 切换 buffer 后也会在后台运行。

* 打开项目

直接使用 C-x C-f 打开任意项目文件。

vterm 不像 eshell, 在 eshell 模式下， cd 到任何目录，再 C-x C-f 会以当前目录作为默认位置让你选择文件。

vterm 则不会，不过如果开启了 emacs server, 则可以通过 emacs-client 来直接打开文件。不过这样的话，当前 vterm 会被卡住。

至于打开项目中的其他文件，可以使用 project-find-file (C-x p f) 来寻找。

* 初始化 git

可选，但是几乎是标配。使用 ~magit-init~ 命令。

#+begin_src emacs-lisp
  (use-package magit
    :straight t
    :bind
    ("C-x g" . magit-status))
#+end_src


* 删除不需要的文件

以下文件是不需要的：

+ counter.js
+ javascript.svg
+ main.js
+ style.css

** 使用 vterm 执行 rm 命令删除

需要执行 ~C-u M-x vterm~ 来启用一个新的 vterm buffer (已经有一个被 vite 占用了）

** 在 dired 模式下批量删除文件

使用 ~C-x d~ 进入 dired 模式，对需要删除的文件按下 d, 然后按下 x 来执行删除。

* 准备 HTML 骨架

** 本地复制 html 到项目根目录

+ 从 eshell 中 cp 目标 html 文件到项目中
+ 使用 dired 实现

** 从已有的服务器上下载 html

通过 ~browse-url-emacs~ 实现

* 删除多余的 html 内容

直接定位到标签上进行删除。调用 web-mode 的 ~C-c C-e k~ 来直接删除整个 tag.

#+begin_src emacs-lisp
  (use-package web-mode
    :straight t
    :mode
    (("\\.html\\'" . web-mode))
    :config
    (setq web-mode-markup-indent-offset 2))
#+end_src

* 引入根目录的 style.scss

** 创建 style.scss

+ 删除原先的 style.css, 再创建一个 style.scss
+ 重命名 style.css -> style.scss 并清空内容

最后的结果： style.css 消失， style.scss 存在  

** 在 html 中引入 styles.scss

+ 保留原来的 style 的 link, 只修改 css 路径
+ 完全删除之前的标签，重新创建一个

最后的结果： 只存在一个指向 style.scss 的 link 标签

* 格式化 HTML 代码

复制过来的 HTML 代码比较乱，需要格式化

* 插入冒烟测试的 css 代码

#+begin_src scss
  body {
      background-color: grey;
  }
#+end_src

** Emmet

一个一个字的输入效率太低，使用 emmet 插件可以提升输入效率：
#+begin_src emacs-lisp
  (use-package emmet-mode
    :straight t
    :config
    (add-hook 'web-mode-hook  'emmet-mode)
    (add-hook 'css-mode-hook  'emmet-mode))
#+end_src

但是 emmet 已经无人维护了，其中也包含了 _Package cl is deprecated_ 这样的警告。

** LSP

所以我打算改用 sass 的 LSP 支持。

https://emacs-lsp.github.io/lsp-mode/page/installation/

#+begin_src emacs-lisp
  (use-package lsp-mode
    :init
    ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
    (setq lsp-keymap-prefix "C-c l")
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
	   (scss-mode . lsp)
	   ;; if you want which-key integration
	   (lsp-mode . lsp-enable-which-key-integration))
    :commands lsp)

  ;; optionally
  (use-package lsp-ui :commands lsp-ui-mode)
  ;; if you are helm user
  (use-package helm-lsp :commands helm-lsp-workspace-symbol)
  ;; if you are ivy user
  (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
  (use-package lsp-treemacs :commands lsp-treemacs-errors-list)

  ;; optionally if you want to use debugger
  (use-package dap-mode)
  ;; (use-package dap-LANGUAGE) to load the dap adapter for your language

  ;; optional if you want which-key integration
  (use-package which-key
    :config
    (which-key-mode))
#+end_src

安装 server:  ~M-x lsp-install-server~

如果 css 没有自动补全，可能是没有启动 company mode

* 引入 styles 目录下的 scss 组件

在 styles.scss 中
#+begin_src scss
  import 'styles';
#+end_src

* 在 index.scss 中进行冒烟测试

把 styles.scss 中的 background 代码剪切过来，可以改一下颜色确保能生效。

这里我发现花括号没有自动补全，可以通过设置

#+begin_src emacs-lisp
  (electric-pair-mode 1)
#+end_src

来实现花括号的自动闭合。

之后就是各个模块的引入了。

+ variables.scss
+ layout.scss
+ element.scss
+ component.scss
+ htmlize.scss

对于变量的补全目前只能根据字符串匹配的方式补全。

* 总结

几个不足和遗憾的点：

+ eshell 对 TUI 支持不好，只能使用 vterm
+ 而 vterm 和 Emacs 集成并不好，体验割裂
+ LSP 对于 scss 的变量补全支持不足

有时候，知道哪里有问题比解决问题更重要。
