#+TITLE: Document Driven Configuration 3
#+SETUPFILE: ./theme.org

* Reset 与 Normalize
借用 CSS 的术语，来纠正一下 Emacs 过时的部分。

Emacs 的年龄比我们都要大，在历史的长河中难免有些不太现代化的东西。

* 设置专门的 custom.el 来存放生成的配置文件
让自动生成的配置放到专门的配置文件中，防止污染 init.el

#+begin_src emacs-lisp
  (setq custom-file (expand-file-name "custom.el" user-emacs-directory))
  (load custom-file)
#+end_src

* 使用 y or n 来回答
默认是 yes or no 来表示 确认/取消。这样可以少打几个字。

#+begin_src emacs-lisp
  (setq-default use-short-answers t)
#+end_src

注意之前比较老的 Emacs 版本，是 ~(defalias 'yes-or-no-p 'y-or-n-p)~

这也是用例驱动的好处，需求是不变的，但是实现（配置代码）会变。

就算不看代码，只要知道回答 y/n 就就可以了。

* 不要生成备份文件
Emacs 会生成 #xxx# 和 xxx~ 这样的历史备份文件，很烦人。

一般本地的版本控制和 undo 机制已经够了。

#+begin_src emacs-lisp
  (setq make-backup-files nil)
  (setq auto-save-default nil)
  (setq backup-inhibited t)
  (setq backup-directory-alist `(("." . "~/.saves")))
#+end_src

* 精简 UI, 扩大可视面积
不要工具栏，不要滚动条，不要菜单栏。

#+begin_src emacs-lisp
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (menu-bar-mode -1)
#+end_src
* 删除文件时使用系统的回收站
直接删除风险太大，使用回收站保险一点。

#+begin_src emacs-lisp
  (setq-default delete-by-moving-to-trash t)
#+end_src

* 自动恢复 buffer
现代化的文本编辑器在检测到文件因为别的原因而被修改时都会进行同步。

Emacs 当然也支持，只要一行配置。

#+begin_src emacs-lisp
  (global-auto-revert-mode t)
#+end_src

* 当退出时，更友好的提示未保存的文件
#+begin_src emacs-lisp
  (defun clean-exit ()
    "Exit Emacs cleanly. If there are unsaved buffer, pop up a list for them to be saved before existing.
  Replaces ‘save-buffers-kill-terminal’."
    (interactive)
    (if (frame-parameter nil 'client)
        (server-save-buffers-kill-terminal arg)
      (if-let ((buf-list (seq-filter (lambda (buf)
                                       (and (buffer-modified-p buf)
                                            (buffer-file-name buf)))
                                     (buffer-list))))
          (progn
            (pop-to-buffer (list-buffers-noselect t buf-list))
            (message "s to save, C-k to kill, x to execute"))
        (save-buffers-kill-emacs))))
  (global-set-key (kbd "C-x C-c") 'clean-exit)
#+end_src
