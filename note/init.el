(defun hello-world()
  "Say hello world"
  (interactive)
  (message "Hello World"))

(org-babel-load-file "~/code/emacs-weekly/prosthetic-arm/config.org")
