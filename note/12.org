#+TITLE: Document Driven Configuration 12
#+SETUPFILE: ./theme.org

* minibuffer
The minibuffer is a special buffer for entering commands or input arguments to commands.

It appears either in a separate, standalone frame or at the bottom of each frame.

* minibuffer 也是 buffer
buffer 中的很多操作也可以在 minibuffer 中使用。

例如： ~C-a~  ~C-e~  ~C-b~  ~C-f~

如果要换行的话，可以按下 ~C-q C-j~

* minibuffer 不一定是 minibuffer
Sometimes, the area where you input commands is used to display informative messages without requesting input (i.e., not just a prompt).

When this area is used this way, it is called the EchoArea, not the minibuffer.

#+begin_src emacs-lisp
  (message "call me echo area")
#+end_src

* 自动补全
IDO - Interactively DO things

#+begin_src emacs-lisp
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1)
#+end_src

* 其他用法

显示外部信息

#+begin_src emacs-lisp
  (use-package request
    :straight '(emacs-request
                :host github
                :repo "tkf/emacs-request"))

  (require 'request)
  (require 'json)

  (defvar random-message nil
    "Displayed on echo area")

  (defun random-message-start()
    (interactive)
    (run-at-time "0 sec"
                 3
                 #'random-message-fetch)
    (random-message-update-status))

  (defun random-message-fetch()
    (interactive)
    (request "http://httpbin.org/get"
      :parser 'json-read
      :success (cl-function
                (lambda (&key data &allow-other-keys)
                  (let ((new-message (assoc-default 'X-Amzn-Trace-Id (assoc-default 'headers data))))
                    (if (can-display-random-message)
                        (display-message-and-store-random-message new-message)
                      ))))))

  (defun can-display-random-message ()
    (and
     (or
      (eql (current-message) nil)
      (equal (current-message) random-message)
      )
     (eql (minibuffer-prompt) nil)))

    (defun display-message-and-store-random-message(new-message)
      (setq random-message new-message)
      (message new-message)
      )

    (defun random-message-update-status()
      (message random-message))
#+end_src
