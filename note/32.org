#+TITLE: Document Driven Configuration 32
#+SETUPFILE: ./theme.org

* 自动输入

* autoinsert

新创建文件的时候插入内容, 比如新建一个 html 文件就插入基本的模版

#+begin_src elisp
  (use-package autoinsert
    :init
    (setq auto-insert-query nil) ;; Don't want to be prompted before insertion:
    (setq auto-insert-directory (locate-user-emacs-file "auto-insert-templates"))
    (auto-insert-mode 1)
    :config
    (define-auto-insert "\.html?$" "default-html.html"))
#+end_src

这个功能不太灵活, 用的比较少。

* Yasnippet

安装

#+begin_src elisp
  (use-package yasnippet
    :hook
    ((text-mode
      prog-mode
      org-mode) . yas-minor-mode)
    :config
    (add-to-list 'yas-snippet-dirs (locate-user-emacs-file "snippets"))
    (yas-global-mode 1))
#+end_src

默认展开快捷键是 TAB (~yas-expand~)

~yas-snippet-dirs~ 可以用软链接到 dropbox 之类的地方, 因为这个内容还是比较私人的, 用软链接可以解耦具体的云同步工具

** 例子

快速输入 elisp 配置代码中的 ~use-package~ , 使用 ~yas-new-snippet~ 来创建。

本质就是在一个特定的目录下创建一个文件, 所以熟练以后可以直接编辑

快速输入 ~use-package~

#+begin_example
# -*- mode: snippet -*-
# name: use-package
# key: up1
# --

(use-package $1
)
#+end_example

注意 key, 它是会根据当前 buffer 的主模式进行分类的, 所以 key 是可以重复的(不同模式下, 不同文件夹)

** 默认值

#+begin_example
# -*- mode: snippet -*-
# name: use-package
# key: up2
# --

(use-package ${1: package-name}
)
#+end_example

这里设置了一个默认值叫 package-name

** 镜像

#+begin_example
# -*- mode: snippet -*-
# name: use-package
# key: up3
# --
;; install package ${1: package-name}

(use-package $1
)
#+end_example

输入的名字会同时出现在注释和代码中 (两个 $1) 的地方
** 嵌套

#+begin_example
# -*- mode: snippet -*-
# name: use-package
# key: up4
# --
(use-package ${1: package-name}
  ${2: :config
  ${3:values} }
)

#+end_example

光看代码可能难理解, 实战一下就知道了。

如果我不需要配置这个 package, 就直接按下删除键就整个删除可能的选项部分, 否则可以输入配置内容

** 嵌入 elisp 代码

#+begin_example
# -*- mode: snippet -*-
# name: use-package
# key: up5
# --
;; created at `(current-time-string)`

(use-package ${1$(downcase yas-text)}
)

#+end_example

+ 直接获取一个表达式的值 (当前时间)
+ 修改输入的内容 (小写包名)
  
** 快捷键

大多数情况下, 除了非常非常常用的 snippet, 没必要专门为一个 snippet 设置一个快捷键。

不过有时候会碰到不用快捷键无法实现的情况, 比如下面这个包裹代码的情况。

例: 在 web 开发中, 假设前端提供了一个静态的 HTML 内容, 比如:

#+begin_src html
  <div class="item">
    something
  </div>
#+end_src

我们需要在 erb (或任何模版格式)中, 加入一个循环来展示后端提供的数据, 使其成为:

#+begin_src html
  <% items.each do |item| %>
  <div class="item">
    something
  </div>
  <% end %>
#+end_src

这个时候, 就可以这样配置:

#+begin_example
# -*- mode: snippet -*-
# name: erb loop
# key: loop
# binding: C-c C-c l
# --
<% $1.each do |$2| %>
  `yas-selected-text`
<% end %>
#+end_example

注意 ~yas-selected-text~ 表示调用时, 被选中的文字。

* 新建文件时调用 yasnippet

#+begin_src emacs-lisp
  (defun my-after-file-template ()
    (when (and (buffer-file-name)
  	     (equal (file-name-extension (buffer-file-name)) "el")
               (not (file-exists-p (buffer-file-name))))
      (let* ((file-extension (file-name-extension (buffer-file-name)))
             (snippet-name "up5"))
        (when (yas-lookup-snippet snippet-name)
  	(progn
  	  (insert snippet-name)
  	  (yas-expand)
  	  (insert "abc") ;; 没有找到 yas-expand 时可以传入的参数, 只能暴力使用 insert 了
  	  )))))

   (add-hook 'find-file-hook 'my-after-file-template)
 #+end_src

这段代码有很多 hardcode, 不过可以跑通整个流程, 就是创建一个新的 .el 文件时, 插入 use-package 为 foobar 的 snippet 

* 动态模版

动态生成 rails 的 controller (CRUD) 的代码 https://guides.rubyonrails.org/getting_started.html#deleting-an-article

选择这个例子是因为 Rails 自带一个 scaffold 可以生成各种代码, 这个例子就可以用 ~rails g controller articles title body~ 来实现。

那么我们就用 snippet 来实现一遍。

要求, 当使用 Emacs 创建一个名为 ~articles_controller.rb~ 的文件时, 自动插入上面链接中的代码, 之后让用户输入相关属性

我们的模板如下:

** 模版

#+begin_example
  # key: rails_controller
  # name: rails_controller
  # --

  class ${1:$(capitalize (inflection-pluralize-string yas-text))}Controller < ApplicationController
      def index
        @${1:$(inflection-pluralize-string yas-text)} = ${1:$(capitalize yas-text)}.all
      end

      def show
        @$1 = ${1:$(capitalize yas-text)}.find(params[:id])
      end

      def new
        @$1 = ${1:$(capitalize yas-text)}.new
      end

      def create
        @$1 = ${1:$(capitalize yas-text)}.new($1_params)

        if @$1.save
          redirect_to @$1
        else
          render :new, status: :unprocessable_entity
        end
      end

      def edit
        @$1 = ${1:$(capitalize yas-text)}.find(params[:id])
      end

      def update
        @$1 = ${1:$(capitalize yas-text)}.find(params[:id])

        if @$1.update($1_params)
          redirect_to @$1
        else
          render :edit, status: :unprocessable_entity
        end
      end

      def destroy
        @$1 = ${1:$(capitalize yas-text)}.find(params[:id])
        @$1.destroy

        redirect_to root_path, status: :see_other
      end

      private
        def $1_params
          params.require(:$1).permit(${2:attrs})
        end
  end
#+end_example

注意这里的一个 resource (article) 有下面三种形态:

+ article
+ articles
+ Article

另外还有 title 和 body 这样的可选参数

总体情况相对而言比较复杂, 所以我们需要先创建帮助函数:

+ 单复数的切换
+ 开头字母大写

** 帮助函数的实现

单复数的切换

#+begin_src emacs-lisp
  (use-package inflections)

  (inflection-pluralize-string  "article") ;; articles
  (inflection-singularize-string "articles") ;; article
#+end_src

开头字母大写

#+begin_src emacs-lisp
  (capitalize "article") ;; Article
#+end_src

** 新建 controller 文件的钩子

把之前 hardcode 的代码拿来改一下。

#+begin_src emacs-lisp  
  (defun scaffold-content (snippet-name default-value)
    (when (and (buffer-file-name)
               (not (file-exists-p (buffer-file-name))))
      (when (yas-lookup-snippet snippet-name)
        (progn
  	(insert snippet-name)
  	(yas-expand)
  	(insert default-value)
  	))))

  (defun rails-controller-hook ()
    (when (string-match-p "_controller\\.rb\\'" buffer-file-name)
      (let ((resources-name  (replace-regexp-in-string "_controller.rb" "" (file-name-nondirectory buffer-file-name))))
        (apply #'scaffold-content (list "rails_controller" (inflection-singularize-string resources-name))))))

  (add-hook 'find-file-hook 'rails-controller-hook)
#+end_src

注意我们创建的 controller 的文件名是复数形式(如: ~books_controller~) 所以需要先转成单数形式再传给 scaffold-content

最后添加 ~find-file~ 的钩子, 注意不要滥用钩子, 否则出现问题很难查找。

* 总结

在编程(或者其他需要输入文字的情况), 难免需要输入很多模版内容, 把这部分内容自动化会极大的提高文字处理的效率。

而 Emacs 强大的可配置性在这方面有很大的操作空间, 是一个值得探究的主题。
