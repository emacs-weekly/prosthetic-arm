#+TITLE: Document Driven Configuration 4
#+SETUPFILE: ./theme.org

* 键盘配置
操作系统层面交换 Capslock 和 ctrl

[[https://media.karousell.com/media/photos/products/2021/6/1/ikbc_c87_mechanical_keyboard_c_1622537228_c4ddce0d_progressive.jpg]]
** AppleII 的键盘
[[https://i0.wp.com/www.apple2history.org/wp-content/uploads/2018/07/Apple-II-keyboard.jpg?w=1396&ssl=1]]
** HHKB
[[https://duet-cdn.vox-cdn.com/thumbor/0x0:4200x2800/2400x1600/filters:focal(2100x1400:2101x1401):format(webp)/cdn.vox-cdn.com/uploads/chorus_asset/file/19571089/HHKB_Pro_3_Classic_Charcoal_printed.jpg]]
* 把键盘上的 Alt 映射为 Meta 键
#+begin_src emacs-lisp :lexical no
  (setq mac-command-modifier 'meta
	mac-option-modifier 'meta)
#+end_src

* 关于 CUA mode
Common User Access mode

禁用 ctrl-c
#+begin_src emacs-lisp
  (global-unset-key (kbd "M-c"))
#+end_src

* caps lock

#+begin_src emacs-lisp
  (use-package caps-lock
    :straight t)
#+end_src

* Pyim

#+begin_src emacs-lisp
  (use-package pyim
    :straight t
    :demand t
    :config
    (use-package pyim-basedict
      :straight t
      :config (pyim-basedict-enable))

    (setq default-input-method "pyim")

    (setq-default pyim-english-input-switch-functions
                  '(pyim-probe-dynamic-english
                    pyim-probe-isearch-mode
                    pyim-probe-program-mode
                    pyim-probe-org-structure-template))

    (setq-default pyim-punctuation-half-width-functions
                  '(pyim-probe-punctuation-line-beginning
                    pyim-probe-punctuation-after-punctuation))

    ;;(pyim-isearch-mode 1)
    (setq pyim-page-tooltip 'posframe)
    (setq pyim-page-length 5)

    (setq pyim-dicts
          '((:name "tsinghua"
                   :file "~/Dropbox/Config/pyim-tsinghua-dict.pyim")))

    :bind
    (("M-j" . pyim-convert-string-at-point)))
#+end_src
