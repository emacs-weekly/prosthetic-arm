(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package org
  :ensure t)

(use-package htmlize
  :ensure t
  :config
  (setq org-html-htmlize-output-type 'css)
  (setq org-html-head-include-default-style nil))

(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((ruby . t)
   ))

(use-package inf-ruby
  :ensure t)

(require 'org)
(require 'ox-publish)

(setq org-publish-project-alist
      '(("config-note"
         :base-directory "note"
         :publishing-function org-html-publish-to-html
         :publishing-directory "public/note"
	 :exclude "theme.org"
         :section-numbers nil
         :with-toc nil)))

(provide 'publish)

