(require 'cl-lib)
(if (file-directory-p "~/.emacs.d/straight/build/org/")
    (progn
      (setq load-path (cl-remove-if (lambda (x) (string-match-p "org$" x)) load-path))
      (add-to-list 'load-path (expand-file-name "~/.emacs.d/straight/build/org/"))))
(require 'org)

(org-babel-load-file (expand-file-name "config.org" (expand-file-name user-emacs-directory)))
(org-babel-load-file (expand-file-name "org.org" (expand-file-name user-emacs-directory)))
(org-babel-load-file (expand-file-name "prog.org" (expand-file-name user-emacs-directory)))


