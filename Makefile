all: clean export

export:
	@emacs --batch --no-init-file -l publish.el config.org --funcall org-html-export-to-html
	@emacs --batch --no-init-file -l publish.el org.org --funcall org-html-export-to-html
	@emacs --batch --no-init-file -l publish.el prog.org --funcall org-html-export-to-html
	@emacs --batch --no-init-file -l publish.el --eval "(org-publish-project \"config-note\")"

# clean generated html and org timestamps
clean:
	@rm -rf note/*.html && rm -rf config.html org.html && rm -rf public/note/*.html && rm -rf ~/.org-timestamps/

# remove all the straight content and desktop session
reset:
	@rm -rf elpa/ && rm -rf straight/ && rm -rf .emacs.desktop && rm -rf .emacs.desktop.lock && rm -rf .lsp-session*

