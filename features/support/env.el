(require 'f)

(defvar prosthetic-arm-support-path
  (f-dirname load-file-name))

(defvar prosthetic-arm-features-path
  (f-parent prosthetic-arm-support-path))

(defvar prosthetic-arm-root-path
  (f-parent prosthetic-arm-features-path))

(add-to-list 'load-path prosthetic-arm-root-path)

;; Ensure that we don't load old byte-compiled versions
(let ((load-prefer-newer t))
  (require 'prosthetic-arm)
  (require 'espuds)
  (require 'ert))

(Setup
 ;; Before anything has run
 )

(Before
 ;; Before each scenario is run
 )

(After
 ;; After each scenario is run
 )

(Teardown
 ;; After when everything has been run
 )
