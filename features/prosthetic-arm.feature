Feature: Emacs
  Scenario: OpenConfigFile
    Given I am in buffer "*scratch*"
    When I press "C-c I"
    Then I should be in buffer "config.org"
  
  Scenario: Capslock
    Given I am in buffer "*scratch*"
    When I start an action chain
    And I press "M-x"
    And I type "caps-lock-mode"
    And I execute the action chain
    And I type "bar"
    Then I should see:
    """
    BAR
    """
